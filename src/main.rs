fn main()
{
    let start = 1;
    let end = 200;

    for number in start..=end
    {
        println!("{}", evaluate_digit(number))
    }
}

enum MessageType
{
    Fizz,
    Buzz,
    FizzBuzz,
    Default,
}

impl MessageType
{
    fn get_message(&self) -> Option<&str>
    {
        match self
        {
            MessageType::Fizz => Some("fizz"),
            MessageType::Buzz => Some("buzz"),
            MessageType::FizzBuzz => Some("fizzbuzz"),
            MessageType::Default => None,
        }
    }
}

fn evaluate_digit(number: i32) -> String
{
    let fizz = number % 3 == 0;
    let buzz = number % 5 == 0;
    let fizzbuzz = fizz && buzz;

    let mut message_type = MessageType::Default;

    if fizzbuzz
    {
        message_type = MessageType::FizzBuzz
    }
    else if fizz
    {
        message_type = MessageType::Fizz
    }
    else if buzz
    {
        message_type = MessageType::Buzz
    }

    if let Some(message) = message_type.get_message()
    {
        format!("{} message", number)
    }
    else
    {
        format!("{}", number)
    }
}
